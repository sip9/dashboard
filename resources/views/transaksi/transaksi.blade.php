@extends('layouts.main')

@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title"><a href="/mtambah" class="btn btn-sm btn-primary float-left"><i
                                    class="fas fa-plus"></i>
                                Tambah Data Transaksi</a></div>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                        <table class="table table-head-fixed text-nowrap">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Judul</th>
                                    <th>ISBN</th>
                                    <th>Tgl Pinjam</th>
                                    <th>Tgl Kembali</th>
                                    <th>Status</th>
                                    <th>Biaya</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transaksi as $trs)
                                <tr>
                                    {{-- <td>{{ $mhs->id }}</td> --}}
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $trs->nama }}</td>
                                    <td>{{ $trs->judul_buku }}</td>
                                    <td>{{ $trs->isbn }}</td>
                                    <td>{{ $trs->tanggal_pinjam }}</td>
                                    <td>{{ $trs->tanggal_kembali }}</td>
                                    <td>{{ $trs->status_pinjam }}</td>
                                    <td>{{ $trs->total_biaya }}</td>
                                    <td>
                                        <a href="/tedit/{{ $trs->id }}" class="fas fa-edit"></a>
                                        <a href="/thapus/{{ $trs->id }}" class="fas fa-trash-alt" style="color:red"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>

@endsection