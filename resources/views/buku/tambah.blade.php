@extends('layouts.main')
@section('konten')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $title }}</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-light">
                    <!-- form start -->
                    <form action="/badd" method="post">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Judul Buku</label>
                                <input type="text" class="form-control" name="judul" placeholder="Masukan Judul Buku">
                            </div>
                            <div class="form-group">
                                <label for="">Pengarang</label>
                                <input type="text" class="form-control" name="pengarang"
                                    placeholder="Masukan Pengarang">
                            </div>
                            <div class="form-group">
                                <label for="">Penerbit</label>
                                <input type="text" class="form-control" name="penerbit" placeholder="Masukan Penerbit">
                            </div>
                            <div class="form-group">
                                <label for="">Tahun Terbit</label>
                                <input type="number" class="form-control" name="tahun"
                                    placeholder="Masukan Tahun Terbit">
                            </div>
                            <div class="form-group">
                                <label for="">Tebal</label>
                                <input type="text" class="form-control" name="tebal" placeholder="Masukan Tebal">
                            </div>
                            <div class="form-group">
                                <label for="">ISBN</label>
                                <input type="text" class="form-control" name="isbn" placeholder="Masukan ISBN">
                            </div>
                            <div class="form-group">
                                <label for="">Stok Buku</label>
                                <input type="number" class="form-control" name="stok" placeholder="Masukan Stok Buku">
                            </div>
                            <div class="form-group">
                                <label for="">Biaya Sewa Harian</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                    </div>
                                    <input type="text" class="form-control" name="biaya"
                                        placeholder="Masukan Biaya Sewa Harian">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="row">
                                <div class="form-group pl-3">
                                    <a href="/buku" class="btn btn-danger">Batal</a>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection