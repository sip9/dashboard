<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\TransaksiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'dashboard']);

#Route Data Mahasiswa
Route::get('/mahasiswa', [MahasiswaController::class, 'mahasiswa']);
Route::get('/mtambah', [MahasiswaController::class, 'tambah']);
Route::post('/madd', [MahasiswaController::class, 'add']);
Route::get('/medit/{id}', [MahasiswaController::class, 'edit']);
Route::post('/mupdate', [MahasiswaController::class, 'update']);
Route::get('/mhapus/{id}', [MahasiswaController::class, 'hapus']);

#Route Data Buku
Route::get('/buku', [BukuController::class, 'buku']);
Route::get('/btambah', [BukuController::class, 'tambah']);
Route::post('/badd', [BukuController::class, 'add']);
Route::get('/bedit/{id}', [BukuController::class, 'edit']);
Route::post('/bupdate', [BukuController::class, 'update']);
Route::get('/bhapus/{id}', [BukuController::class, 'hapus']);

#Route Data Transaksi
Route::get('/transaksi', [TransaksiController::class, 'transaksi']);
Route::get('/ttambah', [TransaksiController::class, 'tambah']);
Route::post('/tadd', [TransaksiController::class, 'add']);
Route::get('/tedit/{id}', [TransaksiController::class, 'edit']);
Route::post('/tupdate', [TransaksiController::class, 'update']);
Route::get('/thapus/{id}', [TransaksiController::class, 'hapus']);