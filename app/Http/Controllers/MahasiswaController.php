<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function mahasiswa(){
        $mahasiswa = DB::table('mahasiswa')->get();
		return view('mahasiswa.mahasiswa',[
            'mahasiswa' => $mahasiswa,
            "title" => "Data Mahasiswa"
        ]);
    }
    public function tambah(){
		return view('mahasiswa.tambah',[
            "title" => "Tambah Data Mahasiswa"
        ]);
    }
    public function add(Request $request)
	{
		DB::table('mahasiswa')->insert([
			'nama' => $request->nama,
			'nim' => $request->nim,
			'email' => $request->email,
            'no_telp' => $request->no,
			'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
			'fakultas' => $request->fakultas
		]);
		return redirect('/mahasiswa');
	}

    public function edit($id)
	{
		$mahasiswa = DB::table('mahasiswa')->where('id',$id)->get();
		return view('mahasiswa.edit',[
            'mahasiswa' => $mahasiswa,
            "title" => "Edit Data Mahasiswa"
        ]);
	}
	public function update(Request $request)
	{
		DB::table('mahasiswa')->where('id',$request->id)->update([
			'nama' => $request->nama,
			'nim' => $request->nim,
			'no_telp' => $request->no,
			'prodi' => $request->prodi,
            'jurusan' => $request->jurusan,
			'fakultas' => $request->fakultas,
		]);
		return redirect('/mahasiswa');
	}

	public function hapus($id)
	{
		DB::table('mahasiswa')->where('id',$id)->delete();
		return redirect('/mahasiswa');
	}
}
