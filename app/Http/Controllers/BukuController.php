<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    public function buku(){
        $buku = DB::table('buku')->get();
		return view('buku.buku',[
            'buku' => $buku,
            "title" => "Data Buku"
        ]);
    }
    public function tambah(){
		return view('buku.tambah',[
            "title" => "Tambah Data Buku"
        ]);
    }
    public function add(Request $request)
	{
		DB::table('buku')->insert([
			'judul_buku' => $request->judul,
			'pengarang' => $request->pengarang,
			'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun,
			'tebal' => $request->tebal,
            'isbn' => $request->isbn,
			'stok_buku' => $request->stok,
            'biaya_sewa_harian' => $request->biaya
		]);
		return redirect('/buku');
	}

    public function edit($id)
	{
		$buku = DB::table('buku')->where('id',$id)->get();
		return view('buku.edit',[
            'buku' => $buku,
            "title" => "Edit Data Buku"
        ]);
	}
	public function update(Request $request)
	{
		DB::table('buku')->where('id',$request->id)->update([
			'judul_buku' => $request->judul,
			'pengarang' => $request->pengarang,
			'penerbit' => $request->penerbit,
            'tahun_terbit' => $request->tahun,
			'tebal' => $request->tebal,
            'isbn' => $request->isbn,
			'stok_buku' => $request->stok,
            'biaya_sewa_harian' => $request->biaya
		]);
		return redirect('/buku');
	}

	public function hapus($id)
	{
		DB::table('buku')->where('id',$id)->delete();
		return redirect('/buku');
	}
}
