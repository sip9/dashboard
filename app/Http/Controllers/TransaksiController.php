<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    public function transaksi(){
        $transaksi = DB::table('transaksi')
        ->join('mahasiswa', 'transaksi.id_mahasiswa', '=', 'mahasiswa.id')
        ->join('buku', 'transaksi.id_buku', '=', 'buku.id')
        ->select('transaksi.*', 'mahasiswa.nama', 'buku.judul_buku', 'isbn')
        ->get();
		return view('transaksi.transaksi',[
            'transaksi' => $transaksi,
            "title" => "Data transaksi"
        ]);
    }
}
